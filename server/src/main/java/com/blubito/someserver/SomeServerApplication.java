package com.blubito.someserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SomeServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SomeServerApplication.class, args);
	}

}
