package com.blubito.someserver.web;

import com.blubito.someserver.domain.Foo;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/foo")
public class FooResource {

    @GetMapping
    public ResponseEntity<Foo> getFoo() {
        Foo response = new Foo();
        response.setBar("Too foo or not to foo?");
        return ResponseEntity.ok().body(response);
    }
}
