package com.blubito.someclient;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLException;
import javax.net.ssl.TrustManagerFactory;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.springframework.web.reactive.function.client.WebClient;

import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

	private static String keyStoreName = "classpath:clientKeyStore.p12";
	private static String keyStorePassword = "123456";
    private static String trustStoreName = "classpath:clientTrustStore.p12";
    private static String trustStorePassword = "123456";

    public ApplicationStartup() {
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        try {
            final SslContext sslContext = getTwoWaySslContext();
            HttpClient httpClient = HttpClient.create().secure(sslSpec -> sslSpec.sslContext(sslContext));

            WebClient webClient = WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .build();

            Mono<Foo> fooMono = webClient
            .get()
            .uri("https://localhost:8083/foo")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToMono(Foo.class);

            Foo foo = fooMono.block();

            System.out.println(foo.getBar());
        } catch(SSLException ex) {
            ex.printStackTrace();
        }

    }

    public static SslContext getTwoWaySslContext() throws SSLException {
        return SslContextBuilder.forClient()
                .keyManager(keyManagerFactory())
                .trustManager(trustManagerFactory())
                .build();
    }

	private static KeyManagerFactory keyManagerFactory() {
        try {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            InputStream p12 = new FileInputStream(ResourceUtils.getFile(keyStoreName));
            keyStore.load(p12, keyStorePassword.toCharArray());

            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            keyManagerFactory.init(keyStore, keyStorePassword.toCharArray());

            return keyManagerFactory;

        } catch (Exception e) {
            throw new RuntimeException("Error creating SSL context (KeyManagerFactory).", e);
        }
    }

    private static TrustManagerFactory trustManagerFactory() {
        try {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            InputStream p12 = new FileInputStream(ResourceUtils.getFile(trustStoreName));
            keyStore.load(p12, trustStorePassword.toCharArray());

            TrustManagerFactory trustManagerFactory =
            TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(keyStore);

            return trustManagerFactory;

        } catch (Exception e) {
            throw new RuntimeException("Error creating SSL context (TrustManagerFactory).", e);
        }
    }

}
