# Introduction

The sample code provided here displays the basic usage of authentication based on client certificates / public keys.

# Basic terminology

## Private / Public keys
The concept of private and public keys is used heavy within the field of encryption. The interesting aspect of private and public keys is that they behave very differently from one another.

A public key can ENCRYPT a payload which the private key can then DECRYPT. This doesnt work the other way around

On the other hand its possible that the private key can ENCRYPT something that the public key can then VERIFY to be authentic (as in, it was encrypted by this specific private key). It cannot decrypt however

This works by black magic. Whoever says something else is a wizard and should not be trusted.

## Certificates
A certificate is essentially a file that matches a key to an entity (for example a domain or a client). It adds various meta information (like common name, organization, ...) to the key.

## PKCS12
PKCS12 files are containers that contain certificates (1 or multiple). The files themselves can be encrypted with a password and can house both public and/or private keys for both keystores and truststores.
PKCS12 files are a more universal standard, replacing the now obsolete JKS format (which was java-specific.)

## KeyStore
In this context the keystore contains the certificate that our java application uses to authenticate itself (instead of using, for example, a username&password)

## TrustStore
This is what a java application uses to verify the identity of OTHERS. In our case, the clients truststore will contain the "identity" (public certificate) of the server. The servers trust store in turn contains the public certificate of the client.
Java runtimes come with an existing truststore out-of-the-box which already contain various trusted parties. This is generally sufficient when you're dealing with "real" (not self-signed) certificates - but when we're dealing with certificates that we created ourselves (for development), its possible to build our own truststore.


# Applications
## Server
The server itself is pretty basic, because all of the heavy-lifting is essentially done by Spring.
By setting the correct values for trust- and keystore within the [application.properties](./server/src/main/resources/application.properties) - spring correctly configures our application to only accept connections that are authenticated both with our keystore as well as with a corresponding public key that is present in our trust store


## Client
The client is a little bit more challenging since we have to construct our own ssl-context. The ssl-context contains our own keystore and truststore, both loaded from files within the classpath.
The heavy lifting for this happens within [ApplicationStartup.java](./client/src/main/java/com/blubito/someclient/ApplicationStartup.java)


# Run the app
Simply start the server and then the client by starting the corresponding maven scripts.
